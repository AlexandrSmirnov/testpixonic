using Data;
using UnityEngine;

namespace View
{
    public class PlanetView : BaseSpaceObjectView
    {
        private readonly Color _normalViewColor = new Color(1f, 1f, 1f, 1f);
        private readonly Color _specialViewColor = new Color(0.5f, 1f, 0.7f, 1f);

        [SerializeField] private SpriteRenderer _renderer;

        private PlanetData _planetData;

        public PlanetData PlanetData
        {
            get => _planetData;
            set
            {
                _planetData = value;
                Rating = _planetData.Rating;
            }
        }

        public bool IsSpecialView
        {
            set => _renderer.color = value ? _specialViewColor : _normalViewColor;
        }
    }
}