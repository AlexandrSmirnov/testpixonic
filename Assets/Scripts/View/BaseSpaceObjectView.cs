using TMPro;
using UnityEngine;

namespace View
{
    public abstract class BaseSpaceObjectView : MonoBehaviour
    {
        [SerializeField] private TextMeshPro _ratingText;

        public int Rating
        {
            set => _ratingText.text = "" + value;
        }
    }
}