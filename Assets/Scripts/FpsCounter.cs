using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class FpsCounter : MonoBehaviour
{
    [SerializeField] private float _updateInterval = 0.1f;

    private TextMeshProUGUI _textMesh;

    private float _totalFrames;
    private float _timePassed;

    private void Awake()
    {
        _textMesh = GetComponent<TextMeshProUGUI>();
        Application.targetFrameRate = 60;
    }

    private void Update()
    {
        _totalFrames++;
        _timePassed += Time.deltaTime;

        if (!(_timePassed >= _updateInterval))
        {
            return;
        }

        var fps = _totalFrames / _timePassed;
        _totalFrames = 0;
        _timePassed = 0;

        var fpsString = ((int) fps).ToString();
        _textMesh.text = fpsString;
    }
}