using System.Collections.Generic;
using Data;

public static class GameExtensions
{
    public static void AddLine(this List<Coordinates> result, Coordinates start, Coordinates direction, int length)
    {
        while (length > 0)
        {
            result.Add(start);
            start += direction;
            length--;
        }
    }
}