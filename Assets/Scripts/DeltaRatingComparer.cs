using System.Collections.Generic;
using UnityEngine;

public class DeltaRatingComparer : IComparer<int>
{
    public int Compare(int rating1, int rating2)
    {
        return rating1 - rating2;
    }
}