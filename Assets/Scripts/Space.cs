using System.Collections;
using System.Collections.Generic;
using Data;
using TMPro;
using UnityEngine;
using View;

public class Space : MonoBehaviour
{
    private const float PlanetProbability = 0.3f;

    private const int MinRating = 0;
    private const int MaxRating = 10000;

    private const int MinVisibilitySize = 5;
    private const int MaxVisibilitySize = 10000;

    private const int SpecialViewFromVisibilitySize = 10;
    private const int SpecialViewVisiblePlanets = 20;

    private const int NominalCameraSize = 5;
    private const float CheckInputDelay = 0.02f;

    public static int SpaceshipRating;

    [SerializeField] private SpaceshipView _spaceshipView;
    [SerializeField] private GameObject _planetPrefab;
    [SerializeField] private Camera _camera;
    [SerializeField] private TextMeshProUGUI _visibilitySizeText;

    private readonly SortedSet<PlanetData> _sortedPlanetData = new SortedSet<PlanetData>(new PlanetsComparer());
    private readonly Dictionary<Coordinates, PlanetView> _normalPlanetViewsByCoordinates = new Dictionary<Coordinates, PlanetView>();
    private readonly List<PlanetView> _specialPlanetViews = new List<PlanetView>();

    private GameObjectPool _planetViewsPool;
    private Coordinates _spaceshipCoordinates;
    private int _visibilitySize;
    private int _visibilityRange;
    private bool _isSpecialView;
    private Vector3 _objectsScale;
    private Coroutine _inputCoroutine;


    private void Awake()
    {
        SpaceshipRating = Random.Range(MinRating, MaxRating);

        _spaceshipView.Rating = SpaceshipRating;

        _planetViewsPool = new GameObjectPool(_planetPrefab, transform);
        _spaceshipCoordinates = new Coordinates();

        SetVisibilitySize(MinVisibilitySize);
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.W))
            StepUp();
        else if (Input.GetKey(KeyCode.A))
            StepLeft();
        else if (Input.GetKey(KeyCode.S))
            StepDown();
        else if (Input.GetKey(KeyCode.D))
            StepRight();
        else if (Input.GetKey(KeyCode.Plus) || Input.GetKey(KeyCode.KeypadPlus))
            ZoomIn();
        else if (Input.GetKey(KeyCode.Minus) || Input.GetKey(KeyCode.KeypadMinus))
            ZoomOut();

        return;

        if (Input.anyKeyDown && _inputCoroutine == null)
        {
            _inputCoroutine = StartCoroutine(CheckInput());
            return;
        }

        if (!Input.anyKey && _inputCoroutine != null)
        {
            StopCoroutine(_inputCoroutine);
            _inputCoroutine = null;
        }
    }

    private IEnumerator CheckInput()
    {
        var wait = new WaitForSeconds(CheckInputDelay);

        while (true)
        {
            if (Input.GetKey(KeyCode.W))
                StepUp();
            else if (Input.GetKey(KeyCode.A))
                StepLeft();
            else if (Input.GetKey(KeyCode.S))
                StepDown();
            else if (Input.GetKey(KeyCode.D))
                StepRight();
            else if (Input.GetKey(KeyCode.Plus) || Input.GetKey(KeyCode.KeypadPlus))
                ZoomIn();
            else if (Input.GetKey(KeyCode.Minus) || Input.GetKey(KeyCode.KeypadMinus))
                ZoomOut();

            yield return wait;
        }
    }

    public void StepUp()
    {
        MoveSpaceshipByStep(Coordinates.Top);
    }

    public void StepLeft()
    {
        MoveSpaceshipByStep(Coordinates.Left);
    }

    public void StepRight()
    {
        MoveSpaceshipByStep(Coordinates.Right);
    }

    public void StepDown()
    {
        MoveSpaceshipByStep(Coordinates.Bottom);
    }

    public void ZoomIn()
    {
        SetVisibilitySize(_visibilitySize - 1);
    }

    public void ZoomOut()
    {
        SetVisibilitySize(_visibilitySize + 1);
    }

    private void MoveSpaceshipByStep(Coordinates direction)
    {
        var length = _visibilityRange * 2 + 1;
        var coordinatesList = new List<Coordinates>();

        // оптимизация
        // не нужно перебирать всю видимую область и область с которой ушли
        // нужно обработать только края с обеих сторон движения

        // сначала вернем в пул плнеты с линии, с которой ушли
        // потом добавим (если необходимо) новые планеты на новой линии 

        if (direction.X != 0)
        {
            // переместились по горизонтали

            coordinatesList.AddLine(
                new Coordinates(_spaceshipCoordinates.X - _visibilityRange * direction.X,
                    _spaceshipCoordinates.Y - _visibilityRange),
                Coordinates.Top, length);

            RemovePlanetsAtCoordinates(coordinatesList);

            coordinatesList.Clear();

            coordinatesList.AddLine(
                new Coordinates(_spaceshipCoordinates.X + _visibilityRange * direction.X + direction.X,
                    _spaceshipCoordinates.Y - _visibilityRange),
                Coordinates.Top, length);

            CreatePlanetDataAtCoordinates(coordinatesList);
        }
        else if (direction.Y != 0)
        {
            // переместились по вертикали

            coordinatesList.AddLine(
                new Coordinates(_spaceshipCoordinates.X - _visibilityRange,
                    _spaceshipCoordinates.Y - _visibilityRange * direction.Y),
                Coordinates.Right, length);

            RemovePlanetsAtCoordinates(coordinatesList);

            coordinatesList.Clear();

            coordinatesList.AddLine(
                new Coordinates(_spaceshipCoordinates.X - _visibilityRange,
                    _spaceshipCoordinates.Y + _visibilityRange * direction.Y + direction.Y),
                Coordinates.Right, length);

            CreatePlanetDataAtCoordinates(coordinatesList);
        }

        _spaceshipCoordinates += direction;

        _spaceshipView.transform.position = new Vector3(_spaceshipCoordinates.X, _spaceshipCoordinates.Y);
        _camera.transform.position = new Vector3(_spaceshipCoordinates.X, _spaceshipCoordinates.Y, -10);

        UpdatePlanetsVisible();
        UpdateObjectsScale();
    }

    private void SetVisibilitySize(int newVisibilitySize)
    {
        newVisibilitySize = Mathf.Clamp(newVisibilitySize, MinVisibilitySize, MaxVisibilitySize);
        if (newVisibilitySize == _visibilitySize)
            return;

        _visibilitySize = newVisibilitySize;
        _camera.orthographicSize = _visibilitySize / 2f;
        _visibilitySizeText.text = "" + _visibilitySize;

        CheckSpecialView();

        var newRange = _visibilitySize / 2;
        if (newRange == _visibilityRange)
        {
            UpdateObjectsScale();
            return;
        }

        var absDeltaRange = Mathf.Abs(newRange - _visibilityRange);

        // оптимизация
        // не нужно перебирать всю видимую область
        // нужно обработать лишь измененный периметр - те ячейки, который перестали быть видимыми, и наоборот

        // соберем в список все измененные ячейки
        var coordinatesList = new List<Coordinates>();

        while (absDeltaRange > 0)
        {
            var range = Mathf.Min(_visibilityRange, newRange) + absDeltaRange;
            var length = range * 2;

            // добавим линию вверху слева направо
            coordinatesList.AddLine(
                new Coordinates(_spaceshipCoordinates.X - range, _spaceshipCoordinates.Y + range),
                Coordinates.Right, length);
            // добавим линию справа сверху вниз
            coordinatesList.AddLine(
                new Coordinates(_spaceshipCoordinates.X + range, _spaceshipCoordinates.Y + range),
                Coordinates.Bottom, length);
            // добавим линию снизу справа налево
            coordinatesList.AddLine(
                new Coordinates(_spaceshipCoordinates.X + range, _spaceshipCoordinates.Y - range),
                Coordinates.Left, length);
            // добавим линию слева снизу вверх
            coordinatesList.AddLine(
                new Coordinates(_spaceshipCoordinates.X - range, _spaceshipCoordinates.Y - range),
                Coordinates.Top, length);

            absDeltaRange--;
        }

        var isVisibilityExpand = newRange > _visibilityRange;
        _visibilityRange = newRange;

        if (isVisibilityExpand)
            CreatePlanetDataAtCoordinates(coordinatesList);
        else
            RemovePlanetsAtCoordinates(coordinatesList);

        UpdatePlanetsVisible();
        UpdateObjectsScale();
    }

    private void CheckSpecialView()
    {
        var isSpecialView = _visibilitySize >= SpecialViewFromVisibilitySize;
        if (isSpecialView)
        {
            var specialScale = _camera.orthographicSize / NominalCameraSize;
            _objectsScale = new Vector3(specialScale, specialScale, 1f);
        }
        else
        {
            _objectsScale = Vector3.one;
        }

        if (isSpecialView == _isSpecialView)
            return;

        _isSpecialView = isSpecialView;

        // чтобы потом при каждом изменении границ видимости не заниматься вычислениями, сразу отработаем смену режима

        if (_isSpecialView)
        {
            foreach (var entry in _normalPlanetViewsByCoordinates)
            {
                _planetViewsPool.Free(entry.Value.gameObject);
            }

            _normalPlanetViewsByCoordinates.Clear();

            for (var i = 0; i < SpecialViewVisiblePlanets; i++)
            {
                var planetView = _planetViewsPool.Obtain<PlanetView>();
                _specialPlanetViews.Add(planetView);
            }
        }
        else
        {
            foreach (var planetView in _specialPlanetViews)
            {
                _planetViewsPool.Free(planetView.gameObject);
            }

            _specialPlanetViews.Clear();
        }
    }

    private void CreatePlanetDataAtCoordinates(List<Coordinates> coordinatesList)
    {
        foreach (var coordinates in coordinatesList)
        {
            Random.InitState(coordinates.GetHashCode());

            var isVisiblePlanet = Random.value <= PlanetProbability;
            if (!isVisiblePlanet)
                continue;

            var planetRating = Random.Range(MinRating, MaxRating);
            var planetData = new PlanetData(coordinates, planetRating);
            _sortedPlanetData.Add(planetData);

            // создание вьюшки планеты зависит от режима и контролируется в UpdatePlanetsVisible()
        }
    }

    private void RemovePlanetsAtCoordinates(List<Coordinates> coordinatesList)
    {
        foreach (var coordinates in coordinatesList)
        {
            Random.InitState(coordinates.GetHashCode());

            var isVisiblePlanet = Random.value <= PlanetProbability;
            if (!isVisiblePlanet)
                continue;

            var planetRating = Random.Range(MinRating, MaxRating);
            var planetData = new PlanetData(coordinates, planetRating);
            _sortedPlanetData.Remove(planetData);

            if (_isSpecialView)
                continue;

            var hasPlanetView = _normalPlanetViewsByCoordinates.TryGetValue(coordinates, out var planetView);
            if (hasPlanetView)
            {
                _planetViewsPool.Free(planetView.gameObject);
                _normalPlanetViewsByCoordinates.Remove(coordinates);
            }
        }
    }

    private void UpdatePlanetsVisible()
    {
        if (_isSpecialView)
        {
            // в специальном режиме отображает только первые P планет по отсортированному списку данных
            // остальные существующие планеты должны отправиться в пул

            var index = 0;
//            var planetsCount = _specialPlanetViews.Count;

            foreach (var planetData in _sortedPlanetData)
            {
                if (index >= SpecialViewVisiblePlanets)
                    return;

//                PlanetView planetView;
//
//                if (index >= planetsCount)
//                {
//                    planetView = _planetViewsPool.Obtain<PlanetView>();
//                    _specialPlanetViews.Add(planetView);
//                    planetsCount++;
//                }
//                else
//                {
//                    planetView = _specialPlanetViews[index];
//                }
                
                var planetView = _specialPlanetViews[index];
                planetView.PlanetData = planetData;
                planetView.IsSpecialView = true;
                planetView.transform.position = new Vector3(planetData.Coordinates.X, planetData.Coordinates.Y);

                index++;
            }
        }
        else
        {
            // в обычном режиме должны рисоваться все планеты
            foreach (var planetData in _sortedPlanetData)
            {
                var hasPlanetView = _normalPlanetViewsByCoordinates.TryGetValue(planetData.Coordinates, out var planetView);
                if (!hasPlanetView)
                {
                    planetView = _planetViewsPool.Obtain<PlanetView>();
                    _normalPlanetViewsByCoordinates[planetData.Coordinates] = planetView;
                }

                planetView.PlanetData = planetData;
                planetView.IsSpecialView = false;
                planetView.transform.position = new Vector3(planetData.Coordinates.X, planetData.Coordinates.Y);
            }
        }
    }


    private void UpdateObjectsScale()
    {
        _spaceshipView.transform.localScale = _objectsScale;

        if (_isSpecialView)
        {
            foreach (var entry in _specialPlanetViews)
            {
                entry.transform.localScale = _objectsScale;
            }
        }
        else
        {
            foreach (var entry in _normalPlanetViewsByCoordinates)
            {
                entry.Value.transform.localScale = _objectsScale;
            }
        }

        Debug.Log($"{_sortedPlanetData.Count}");
    }
}