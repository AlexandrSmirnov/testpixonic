using System.Collections.Generic;
using UnityEngine;

public class GameObjectPool
{
    private readonly GameObject _prefab;
    private readonly Transform _parent;
    private readonly Queue<GameObject> _freeObjects = new Queue<GameObject>();

    public GameObjectPool(GameObject prefab, Transform parent)
    {
        _prefab = prefab;
        _parent = parent;
    }

    public GameObject Obtain()
    {
        if (_freeObjects.Count > 0)
        {
            var gameObject = _freeObjects.Dequeue();
            gameObject.SetActive(true);
            return gameObject;
        }

        var newGameObject = Object.Instantiate(_prefab, _parent);
        return newGameObject;
    }

    public T Obtain<T>()
    {
        var gameObject = Obtain();
        var component = gameObject.GetComponent<T>();
        return component;
    }

    public void Free(GameObject gameObject)
    {
        gameObject.SetActive(false);
        _freeObjects.Enqueue(gameObject);
    }
}