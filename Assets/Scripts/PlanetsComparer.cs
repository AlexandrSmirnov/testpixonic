using System.Collections.Generic;
using Data;
using UnityEngine;

public class PlanetsComparer : IComparer<PlanetData>
{
    public int Compare(PlanetData data1, PlanetData data2)
    {
        return Mathf.Abs(Space.SpaceshipRating - data1.Rating) - Mathf.Abs(Space.SpaceshipRating - data2.Rating);
    }
}