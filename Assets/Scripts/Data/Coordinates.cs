namespace Data
{
    public struct Coordinates
    {
        public static readonly Coordinates Top = new Coordinates(0, +1);
        public static readonly Coordinates Bottom = new Coordinates(0, -1);
        public static readonly Coordinates Left = new Coordinates(-1, 0);
        public static readonly Coordinates Right = new Coordinates(+1, 0);

        public int X;
        public int Y;

        public Coordinates(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return $"{nameof(Coordinates)} [{X}, {Y}]";
        }

        public bool Equals(Coordinates other)
        {
            return X == other.X && Y == other.Y;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;

            return obj is Coordinates other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
//                return X ^ Y << 2;
//                return X * 100000 ^ Y;
                return (X * 397) ^ Y;
            }
        }

        public static bool operator ==(Coordinates a, Coordinates b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(Coordinates a, Coordinates b)
        {
            return !(a == b);
        }

        public static Coordinates operator +(Coordinates a, Coordinates b)
        {
            return new Coordinates
            {
                X = a.X + b.X,
                Y = a.Y + b.Y
            };
        }

        public static Coordinates operator -(Coordinates a, Coordinates b)
        {
            return new Coordinates
            {
                X = a.X - b.X,
                Y = a.Y - b.Y
            };
        }
    }
}