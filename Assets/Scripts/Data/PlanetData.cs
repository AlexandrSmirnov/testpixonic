namespace Data
{
    public struct PlanetData
    {
        public readonly Coordinates Coordinates;
        public readonly int Rating;

        public PlanetData(Coordinates coordinates, int rating)
        {
            Coordinates = coordinates;
            Rating = rating;
        }

        public override string ToString()
        {
            return $"{nameof(PlanetData)} [{Coordinates}, {Rating}]";
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return Coordinates.GetHashCode();
                return Coordinates.GetHashCode() * Rating;
                return Coordinates.X * 1000000 + Coordinates.Y;
                return (Coordinates.GetHashCode() * 397) ^ Rating;
            }
        }

        public bool Equals(PlanetData other)
        {
            return Coordinates == other.Coordinates && Rating == other.Rating;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;

            return obj is PlanetData other && Equals(other);
        }

        public static bool operator ==(PlanetData a, PlanetData b)
        {
            return a.Coordinates == b.Coordinates && a.Rating == b.Rating;
        }

        public static bool operator !=(PlanetData a, PlanetData b)
        {
            return !(a == b);
        }
    }
}